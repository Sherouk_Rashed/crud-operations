<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../../autoload.php");

?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
       
    </head>

    <body>
        <div class="container" style="width:550px;padding:10px;background-color:#eceadc;color:#ec8007">
            <div class="form-row">
                <div class="col">
                    <form  action="" method="post" >
                    <div class="form-group col-md-6">
                        <label for="exampleInputFirstName">First Name</label>
                        <input type="text" class="form-control" id="FirstName" placeholder="First Name">
                    </div>
                     <div class="form-group col-md-6">
                        <label for="exampleInputLastName">Last Name</label>
                        <input type="text" class="form-control" id="LastName" placeholder="Last Name">
                    </div>
                     <div class="form-group col-md-6">
                        <label for="exampleInputFirstName">Phone Number</label>
                        <input type="text" class="form-control" id="Phone" placeholder="Phone">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="address_address">First Address</label>
                        <input type="text" id="address-input" name="address_address" class="form-control map-input">
                        <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                        <input type="hidden" name="address_longitude" id="address-longitude" value="0" /><br>
                        <div id="address-map-container" style="width:100%;height:350px; ">
                            <div style="width: 100%; height: 100%" id="address-map"></div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Second Address</label>
                        <input type="email" class="form-control" id="Address"  placeholder="Enter Address">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleFormControlSelect1">Select Country</label>
                        <select name="country" class="countries form-control" id="countryId" >
                              <option value="">Select Country</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleFormControlSelect1">Select State</label>
                        <select name="state" class="states form-control" id="stateId">
                              <option value="">Select State</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleFormControlSelect1">Select City</label>
                        <select name="city" class="cities form-control" id="cityId" >
                              <option value="">Select City</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputFirstName">Post Code</label>
                        <input type="text" class="form-control" id="postcode" placeholder="Postcode">
                    </div>

                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-primary ">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>




        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
        <script src="//geodata.solutions/includes/countrystatecity.js"></script>
        <script type ="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrHiCX0hV2gg6rFKVKDF7i1KC5fBbEC9U&libraries=places&callback=initialize" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type ="text/javascript" src="../JS/mapInput.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        
    </body>

</html>
