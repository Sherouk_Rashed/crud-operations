<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../../autoload.php");

?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Ierek Book Store</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../../Style/css/index.css" />
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
       

    </head>

    <body>

      <!------ Include the above in your HEAD tag ---------->

      <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
        <!-- Overlay -->
        <div class="overlay"></div>

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#bs-carousel" data-slide-to="1"></li>
          <li data-target="#bs-carousel" data-slide-to="2"></li>
        </ol>
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item slides active">
            <div class="slide-1"></div>
            <div class="hero">
              <hgroup>
                  <h1>We are creative</h1>        
                  <h3>Get start your next awesome step</h3>
              </hgroup>
              <a class="btn btn-hero btn-lg" href="view_all_bookings.php">See all Bookings</a>
            </div>
          </div>
          <div class="item slides">
            <div class="slide-2"></div>
            <div class="hero">        
              <hgroup>
                  <h1>We are smart</h1>        
                  <h3>Get start your next awesome step</h3>
              </hgroup>       
              <a class="btn btn-hero btn-lg" href="add_book.php">New Book</a>
            </div>
          </div>
          <div class="item slides">
            <div class="slide-3"></div>
            <div class="hero">        
              <hgroup>
                  <h1>We are amazing</h1>        
                  <h3>Get start your next awesome step</h3>
              </hgroup>
              <a class="btn btn-hero btn-lg" href="#">See all offers</a>
            </div>
          </div>
        </div> 
      </div>


      <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    </body>

</html>
