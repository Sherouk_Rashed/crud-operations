<?php
/**
  * 
  */
class DbHandlerClass implements DbHandler 
{
	private $db_file;
	private $db_dir;
	private $db_handler;
	private $prefix;

    //.
    //.
    //...........Define file of Database..........
    //.
    //.

    function __construct( $db_dir = __DATABASE__FILE__DIR__ , $db_file = __DATABASE__FILE__ , $prefix = '', $connect = true )                                           
 	{		
		//check the file exists
		if( !is_file( $db_dir.'/'.$db_file ) ){
            echo 'SQite3Database: File '.$db_file.' wasn\'t found.';
        }
		//set up database properties
        $this->db_file 	= $db_file;
        $this->db_dir 	= $db_dir;
		$this->prefix	= $prefix;
        if( $connect ){
            $this->connect();
        }
 	}

    //.
    //.
    //...........Establish Connection..........
    //.
    //.
    public function connect(){

        try 
		{
            echo"1\n";
            $this->db_handler = new PDO('sqlite:'.$this->db_dir.'/'.$this->db_file );
            echo"2\n";
		}catch( PDOException $e ){
            echo"3\n";
			throw new Exception( 'SQLite3Database: '.$e->getMessage() );
		}

        $this->db_handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }

    //.
    //.
    //...........Create Book Table..........
    //.
    //.
    public function create_table(){
            $sql =<<<EOF
                CREATE TABLE BOOK
                           (
                            ID         INT       NOT NULL   PRIMARY KEY,
                            FIRSTNAME  CHAR(15)  NOT NULL,
                            LASTNAME   CHAR(15)  NOT NULL,                            
                            PHONE      TEXT      NOT NULL   UNIQUE,
                            EMAIL      TEXT      NOT NULL   UNIQUE,
                            ADDRESSONE CHAR(50)  NOT NULL,
                            LAT        REAL      NOT NULL,
                            LONG       REAL      NOT NULL,
                            ADDRESSTWO CHAR(50)  NOT NULL,
                            CITY       CHAR(50)  NOT NULL,
                            COUNTRY    CHAR(50)  NOT NULL,
                            POSTCODE   INT       NOT NULL,
                            NOTES      TEXT      NOT NULL,
                );
EOF;

        $this->table = $this->db_handler->exec($sql);
        if($this->table){
            echo "Table created successfully\n";
        }else{
            echo $this->db_handler->lastErrorMsg();
        }
        $this->disconnect();
    }


    //.
    //.
    //...........Restore Data Of Books..........
    //.
    //.
    public function get_data($fields = array(),  $start = 0){

    	$table = $this->table;
    	if(empty($fields)){
    		$query = "select * from `$table`";
    	}else{
    		$query = "select ";
    		foreach ($fields as $f) {
    			$query.= "$f ,";
    		}
    		$query.= "from `$table`";
    		$query = str_replace(",from", "from", $query);
    	}
    	//$query.= " limit $start,".__RECORDS_PER_PAGE__;
    	return $this->get_result($query);
    }


    public function disconnect(){

    	if($this->db_handler)
    	{
            $this->db_handler->close();
        }
    }   

    //.
    //.
    //...........Get Book Data By PK..........
    //.
    //.
    public function get_record_by_key($key,$key_value){

    	$table = $this->table;
    	$query = "select * from `$table` where $key = '$key_value'".";";
    	return $this->get_result($query);
    }

    
   
    
    //.
    //.
    //...........Insert New Book..........
    //.
    //.
    public function insert_record($first_name,$last_name,$phone,$email,$address_1,$lat,$long,$address_2,$city,$country,$post_code,$notes)
    {

        $table = $this->table;
        $query = "insert into `$table` 
                  values ($first_name,$last_name,$phone,$email,$address_1,$lat,$long,$address_2,$city,$country,$post_code,$notes)".";" ;
        return $this->get_result($query);
        
    }

    //.
    //.
    //...........Update Record..........
    //.
    //.
    public function update_record($key,$new_value)
    {

        $table = $this->table;
        $query = " update `$table` set $key = '$new_value' ".";";
        return $this->get_result($query);
        
    }
      
    //.
    //.
    //...........Delete Book..........
    //.
    //.
    public function delete_record($key,$key_value)
    {

        $table = $this->table;
        $query = "delete from `$table` 
                  where $key = '$key_value' ".";";
        return $this->get_result($query);
        
    }
    public function get_result($query)
    {

    	if(__DEBUG__MODE__ === 1){
    		echo " Sent Query .. ".$query."<br>";
        }
        $sql = $this->db_handler->prepare($query);
        $handler_result = $sql->execute();
        // var_dump("result of sql excution : ", $handler_result);
        
    	if($handler_result){
    		while ($row = sqlite_fetch_array($handler_result,SQLITE_ASSOC)) {
                $array_result[] = array_change_key_case($row);

            }
            // echo "......";
            // var_dump("Array of Result", $array_result);

            $this->disconnect();
    		return $array_result;
    	}else{
    		$this->disconnect();
    		return false;
    	}

    }


}

?>