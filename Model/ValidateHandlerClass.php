<?php

class ValidateHandlerClass implements ValidateHandler
{

	private $db;
	private $errors; 

	function __construct()                                           
 	{
 		$this->errors = [];
 	}
 	// *
    // *
    // *//*** ....................Validate All Required Inputs
    // *
	public function complete_insertion_updation_enteries($first_name,$last_name,$phone,$email,$address_1,$address_2,$city,$country,$post_code,$notes){
	
		if(!isset($first_name) || trim($first_name) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): First Name Field Required ");
			return;

		}elseif(!isset($last_name) || trim($last_name) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Last Name Field Required ");
			return;

		}elseif(!isset($phone) || trim($phone) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Phone Field Required ");
			return;

		}elseif(!isset($email) || trim($email) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Email Field Required ");
			return;

		}elseif(!isset($address_1) || trim($address_1) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): First Address Field Required ");
			return;

		}elseif(!isset($address_2) || trim($address_2) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Second Address Field Required ");
			return;

		}elseif(!isset($city) || trim($city) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): City Field Required ");
			return;

		}elseif(!isset($country) || trim($country) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Country Field Required ");
			return;

		}elseif(!isset($post_code) || trim($post_code) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Post Code Field Required ");
			return;

		}elseif(!isset($notes) || trim($notes) == ''){
			array_push($this->errors, "Incomplete Inputs (Fields Are Missing): Notes Field Required ");
			return;

		}else{
			  $this->valid_names($firstName,$lastName);            
			  $this->valid_addresses($address_1,$address_2);       
			  $this->valid_email($email);        
			  $this->valid_phone($phone);
			  $this->valid_city($city); 
			  $this->valid_post_code($country,$post_code);    
		}
		
		
		
	}
    // *
    // *
    // *//***.............................. valid Name in size and type
    // *
    public function valid_names($firstName,$lastName){

    	if (!preg_match("/^[a-zA-Z ]*$/",$firstName)){
			array_push($this->errors, "Invalid First Name Structure : First Name Must Be Only Letters");
			
    	}elseif(!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
			array_push($this->errors, "Invalid Last Name Structure : Last Name Must Be Only Letters");
    		
    	}elseif(strlen($firstName) > __MAX__NAME__SIZE__) {
			array_push($this->errors, "Invalid First Name Structure : First Name Must Be Less Than ".__MAX__NAME__SIZE__." Letters");
    		
    	}elseif(strlen($lastName) > __MAX__NAME__SIZE__) {
			array_push($this->errors, "Invalid Last Name Structure : Last Name Must Be Less Than ".__MAX__NAME__SIZE__." Letters");
    		
    	}
    }  
    // *
    // *
    // * //*** .................................valid address in construction
    // *
    public function valid_addresses($address_1,$address_2){

    	if (! (preg_match('/^(?:\\d+ [a-zA-Z ]+, ){2}[a-zA-Z ]+$/', $address_1))){
			array_push($this->errors, "Invalid Address Structure : The First Address Is Not Valid");

		}elseif (! (preg_match('/^(?:\\d+ [a-zA-Z ]+, ){2}[a-zA-Z ]+$/', $address_2))){
			array_push($this->errors, "Invalid Address Structure : The Second Address Is Not Valid");
		}

    }         
    // *
    // *//***..................................... valid Email structure and uniqness
    // *
    // *
    public function valid_email($email){

    	if (! (filter_var($email, FILTER_VALIDATE_EMAIL)) ) {
			array_push($this->errors, "Invalid Email Structure : Email Must Be Valid");
		} 

		$this->db = new DbHandlerClass(); 
		$stored_email = ($this->db)->get_record_by_key("EMAIL",$email);

		if($stored_email){
			array_push($this->errors, "Invalid Email : Email Have Been Used Before");
			($this->db)->disconnect();
		}

	}     

    // *
    // *//*** ...............valid phone valid in type size unique
    // *
    // *
    public function valid_phone($phone){
		
		$this->db = new DbHandlerClass(); 
		$stored_phone = ($this->db)->get_record_by_key("PHONE",$phone);

		if($stored_phone){
			array_push($this->errors, "Invalid Phone : Phone Have Been Used Before");
			($this->db)->disconnect();

		}elseif (! (filter_var($phone, FILTER_SANITIZE_NUMBER_INT)) ) {
			array_push($this->errors, "Invalid Phone Structure : Phone Must Be Valid");

		}elseif(strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
			array_push($this->errors, "Invalid Phone Structure : Phone Must Be Min 7 digits and max 14 digits");
		} 

	}   
	
	//
	//
	//
	//
	//
	//
    public function valid_city($city){
		if( !(preg_match("[a-zA-Z]+(?:[ '-][a-zA-Z]+)*", $city)) ){
			array_push($this->errors, "Invalid City Name : A Valid City Name Structure Is Required");
		}
	} 
	//
	//
	//
	//
	//
	//

    public function valid_post_code($country,$post_code){
		// Read JSON file
		$post_code_json = file_get_contents('postal_codes.json');

		//Decode JSON
		$post_code_json_data = json_decode($post_code_json,true);

		foreach ($post_code_json_data as $key => $value) {
			if( strcasecmp($post_code_json_data[$key]["Country"],$country) == 0 ){
				$my_country_post_code_regex = $json_data[$key]["Regex"];
				if( !(preg_match($my_country_post_code_regex, $post_code)) ){
					array_push($this->errors, "Invalid Post code : Country and post code doesn't match");
				}
			}else{
				array_push($this->errors, "Invalid Country : Country Not Found");
			}
		}
	} 

    // *
    // *..................Retun Errors.............
    // *
    // *
    public function is_Valid($first_name,$last_name,$phone,$email,$address_1,$address_2,$city,$country,$post_code,$notes){

		$this->complete_insertion_updation_enteries($first_name,$last_name,$phone,$email,$address_1,$address_2,$city,$country,$post_code,$notes);
		if(empty($this->errors))
			return true;
		else 
			return false;
    	
    }

	public function get_errors(){
		if(!empty($this->errors))
		{
			return $this->errors;
		}

	}
	

}

?>