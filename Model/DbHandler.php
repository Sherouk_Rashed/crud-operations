<?php
interface DbHandler {
    public function connect();
    public function create_table();
    public function get_data($fields = array(),  $start = 0); //get all books
    public function get_record_by_key($key,$key_value);  // get single record
    public function get_result($sql);
    //...      ...//
    public function insert_record($first_name,$last_name,$phone,$email,$address_1,$lat,$long,$address_2,$city,$country,$post_code,$notes);
    public function update_record($key,$new_value);
    public function delete_record($key,$key_value);
    //...      ...//

    public function disconnect();   
}