<?php
interface ValidateHandler {
	public function complete_insertion_updation_enteries($first_name,$last_name,$phone,$email,$address_1,$address_2,$city,$country,$post_code,$notes);
    public function valid_names($firstName,$lastName);            
    public function valid_addresses($address_1,$address_2);       
    public function valid_email($email);        
    public function valid_phone($phone);  
    public function valid_city($city); 
    public function valid_post_code($country,$post_code);   
    public function is_Valid($first_name,$last_name,$phone,$email,$address_1,$address_2,$city,$country,$post_code,$notes);
    public function get_errors();

}
